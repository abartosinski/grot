(function () {
    var hash = -1,
        gameInProgress = true;
    $(document).ready(function () {
        var $stateElem = $('.state'),
            $notifElem = $('.notif'),
            $playersElem = $stateElem.find('.players'),
            $roundElem = $stateElem.find('.roundNum'),
            $notStartedElem = $stateElem.find('.roundNotStarted'),
            $playerNum = $('.player_num');

        var showState = function (state) {
                $roundElem
                    .toggle(!!state.round)
                    .find('.roundCounter').text(state.round);
                $notStartedElem
                    .toggle(!state.round);
                $playersElem.text('');
                $playerNum.text(state.players.length);

                state.players.slice(0, 30).forEach(function (player, index) {
                    var $playerElem = $('<tr/>');

                    $playerElem.html(
                        '<td class="playerPos">' + (index + 1) + '.</td>' +
                        '<td class="playerName">' + player.name + '</td>' +
                        '<td class="playerScore">'+ player.score + '</td>' +
                        '<td class="playerMoves">moves: ' + player.moves + '</td>'
                    );
                    $playersElem.append($playerElem);

                    if (player.moves === 0) {
                        $playerElem.addClass('inactive');
                    }
                });

                if (!state.is_active) {
                    showInfo('The game has ended.');
                }

                gameInProgress = state.is_active;
            },
            showInfo = function (info) {
                $notifElem.text(info);
                $notifElem.removeClass('error').addClass('info');
            },
            showError = function (error) {
                $notifElem.text(error);
                $notifElem.removeClass('info').addClass('error');
            };

        var getState = function () {
            $.ajax({
                url: '/api/state/' + hash + '/',
                type: 'GET'
            }).success(function (data) {
                var packet = JSON.parse(data),
                    reconnect = true,
                    state;

                if (packet.code === 'data') {
                    state = packet.data.resource;
                    showState(state);
                    hash = packet.data.hash;
                    reconnect = state.is_active;
                } else if (packet.code === 'disconnect' && !packet.running) {
                    reconnect = false;

                    if (gameInProgress) {
                        showError('The server was shut down.');
                    }
                }

                if (reconnect) {
                    setTimeout(getState, 200);
                }
            }).error(function () {
                showError('The connection was terminated.');
            });
        };
        getState();
    });
})();
