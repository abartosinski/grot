import functools
import json
import urllib2
from threading import Event, Thread

from flask import Flask, request

from .resource import ResourceHub


class WebView(object):

    def __init__(self, host, port, contest):
        app = Flask(__name__, static_folder='../static/',
                    static_url_path='/static')

        self.host = host
        self.port = port
        self.contest = contest

        self.running = False
        self.resource_hub = ResourceHub()
        self.resource_hub.register(
            res_name='contest_state',
            get_func=self.contest.get_state,
            hash_func=lambda x: x['round'],
        )

        def shutdown():
            if not self.running and not self.stopped:
                func = request.environ.get('werkzeug.server.shutdown')
                if func is None:
                    raise RuntimeError(
                        'Not running with the Werkzeug Server')
                func()
                self.stopped = True
            return self.stopped

        def check_shutdown(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                ret = func(*args, **kwargs)
                if shutdown():
                    return '', 503
                return ret
            return wrapper

        @app.route('/api/state/<round>/')
        @check_shutdown
        def get_state(round):
            round = json.loads(round)  # int or null
            return self.resource_hub.get('contest_state').get(round)

        @app.route('/')
        @check_shutdown
        def main():
            contest.begin()
            return app.send_static_file('main.html')

        @app.route('/ping/')
        def ping():
            shutdown()
            return '', 200

        self.state = None
        self.state_event = Event()
        self.thread = Thread(target=lambda: app.run(
            threaded=True, host=host, port=port, use_debugger=True))

    def resource_changed(self, res_name):
        self.resource_hub.resource_changed(res_name)

    def start(self):
        self.stopped = False
        self.running = True
        self.thread.start()

    def stop(self):
        print 'Exiting werkzeug...'
        self.running = False
        self.resource_hub.stop()
        urllib2.urlopen('http://{}:{}/ping/'.format(self.host, self.port))
        self.thread.join()
