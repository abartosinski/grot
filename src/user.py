import httplib
import json

from .config import MASTER_CONFIG, PLAYER_CONFIG


class User(object):

    DEFAULT_PORT = 8000
    INVALID_HOSTS = (
        '127.0.0.1',
        'localhost',
        '0.0.0.0',
    )

    def __init__(self, name, address, sync):
        self.name = name
        self.address = self.validate_address(address) if sync else address

    @classmethod
    def validate_address(cls, address):
        if address and address.split(':')[0] in cls.INVALID_HOSTS:
            address = None
        return address

    def sync(self, timeout=5):
        if not MASTER_CONFIG.data:
            return False

        conn = httplib.HTTPConnection(MASTER_CONFIG.data['userdb_address'],
                                      timeout=timeout)
        data = json.dumps({
            'username': self.name,
            'master_password': MASTER_CONFIG.data['userdb_password'],
        })
        headers = {'Content-type': 'application/json'}

        conn.request('POST', '/get_user', data, headers)
        resp = conn.getresponse()
        if resp.status == 200:
            self.address = self.validate_address(
                json.loads(resp.read())['address']
            )

        return self.address is not None

    @classmethod
    def check_conn_all(cls, t):
        return [user.name for user in cls.get_all() if user.check_conn(t)]

    def check_conn(self, t):
        print 'Checking {0}...'.format(self.name)
        if not self.address:
            print 'NOT CONF'
            return False
        try:
            conn = httplib.HTTPConnection(self.address,
                                          timeout=t)
            conn.request('GET', '/connection_test')
            resp = conn.getresponse()
        except:
            print 'FAIL'
            return False
        return True

    def __setattr__(self, name, value):
        if name == 'address' and value and ':' not in value:
            value = '{}:{}'.format(value, self.DEFAULT_PORT)
        super(User, self).__setattr__(name, value)

    @classmethod
    def get_all(cls):
        if PLAYER_CONFIG.data:
            return cls.from_data(PLAYER_CONFIG.data)

        if not MASTER_CONFIG.data:
            return []

        conn = httplib.HTTPConnection(MASTER_CONFIG.data['userdb_address'],
                                      timeout=15)
        data = json.dumps({
            'master_password': MASTER_CONFIG.data['userdb_password'],
        })
        headers = {'Content-type': 'application/json'}
        conn.request('POST', '/sync_users', data, headers)
        resp = conn.getresponse()
        if resp.status == 200:
            return cls.from_data(json.loads(resp.read()), sync=True)

        raise RuntimeError('Could not load users, status code: {}'.format(
            resp.status
        ))

    @classmethod
    def from_data(cls, data, sync=False):
        return [cls(d['name'], d['address'], sync) for d in data]
