import functools
import json
from Queue import Queue
from threading import Lock


def cache(func):
    _cache = {}
    lock = Lock()

    @functools.wraps(func)
    def wrapper(*args):
        with lock:
            if args not in _cache:
                _cache[args] = func()
            return _cache[args]

    def clear():
        with lock:
            _cache.clear()
    wrapper.clear_cache = clear

    return wrapper


class Resource(object):

    MSG_RETURN_DATA = 'data'
    MSG_RECHECK = 'recheck'
    MSG_DISCONNECT = 'disconnect'

    def __init__(self, get_func, hash_func, max_conn):
        self.max_connections = max_conn
        self.get_func = cache(get_func)
        self.hash_func = hash_func

        self.msg_queues = []
        self.is_active = True

    def disconnect_oldest(self):
        try:
            self.msg_queues.pop(0).put(self.MSG_DISCONNECT)
        except IndexError:
            return False
        return True

    def get_msg(self):
        if not self.is_active:
            return self.MSG_DISCONNECT

        queue = Queue()
        self.msg_queues.append(queue)

        if len(self.msg_queues) > self.max_connections:
            self.disconnect_oldest()

        try:
            return queue.get()
        finally:
            if queue in self.msg_queues:
                self.msg_queues.remove(queue)

    def changed(self):
        self.get_func.clear_cache()
        for queue in self.msg_queues:
            queue.put(self.MSG_RECHECK)

    def stop(self):
        self.is_active = False
        while self.disconnect_oldest():
            pass

    def get(self, old_hash):
        """
        Load the resource when its hash is different
        than the one supplied.
        """
        msg = self.MSG_RECHECK

        while msg == self.MSG_RECHECK:
            res = self.get_func()
            new_hash = self.hash_func(res)
            if old_hash != new_hash:
                msg = self.MSG_RETURN_DATA
                break
            msg = self.get_msg()

        if msg == self.MSG_RETURN_DATA:
            data = {
                'resource': res,
                'hash': new_hash,
            }
        else:
            data = None

        return self.make_packet(msg, data)

    def make_packet(self, msg, data):
        return json.dumps({
            'code': msg,
            'data': data,
            'running': self.is_active,
        })


class ResourceHub(object):

    def __init__(self):
        self.resources = {}

    def stop(self):
        for res in self.resources.values():
            res.stop()

    def get(self, res_name):
        return self.resources[res_name]

    def register(self, res_name, get_func, hash_func, max_conn=5):
        if res_name in self.resources:
            raise ValueError('Resource already registered')
        self.resources[res_name] = Resource(get_func, hash_func, max_conn)

    def resource_changed(self, res_name):
        self.get(res_name).changed()
