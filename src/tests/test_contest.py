import json
import os
from datetime import datetime
from tempfile import mkdtemp
from unittest import TestCase

from mock import call, patch

from ..config import MASTER_CONFIG
from ..main import Contest, Player
from ..user import User


class ContestTestCase(TestCase):

    MASTER_CONFIG_DATA = MASTER_CONFIG.data
    CONTEST_DIR = Contest.CONTEST_DIR

    def setUp(self):
        MASTER_CONFIG.data = None
        Contest.CONTEST_DIR = mkdtemp()
        self.users = [
            User('user_{}'.format(x), '1.2.3.4')
            for x in range(50)
        ]

    def tearDown(self):
        MASTER_CONFIG.data = self.MASTER_CONFIG_DATA
        Contest.CONTEST_DIR = self.CONTEST_DIR

    @patch('src.main.Player', spec=Player)
    def test_play_round(self, player):
        contest = Contest(self.users)
        contest.play_round()
        self.assertEqual(
            player().play_round.mock_calls.count(call()),
            50,
        )

        player().play_round.mock_calls = []
        player().is_active.mock_calls = []
        player().is_active.side_effect = lambda: (
            len(player().is_active.mock_calls) <= 10
        )
        contest.play_round()
        self.assertEqual(
            player().play_round.mock_calls.count(call()),
            10,
        )

        player().is_active.mock_calls = []
        self.assertTrue(contest.is_active())

        player().is_active.side_effect = lambda: False
        self.assertFalse(contest.is_active())

    def test_save_winners(self):
        contest = Contest(self.users)
        contest.save_winners()

        saved_winners = os.listdir(Contest.CONTEST_DIR)
        self.assertEqual(len(saved_winners), 1)
        self.assertEqual(
            saved_winners[0].split('T')[0],
            datetime.now().isoformat().split('T')[0],
        )

        filename = os.path.join(
            Contest.CONTEST_DIR,
            saved_winners[0],
        )

        with open(filename, 'r') as f:
            self.assertEqual(json.loads(f.read()), contest.get_state())

        contest.save_winners()
        saved_winners = os.listdir(Contest.CONTEST_DIR)
        self.assertEqual(len(saved_winners), 2)

        os.remove(filename)
        saved_winners = os.listdir(Contest.CONTEST_DIR)

        self.assertEqual(
            saved_winners[0].split('T')[0],
            datetime.now().isoformat().split('T')[0],
        )

        filename = os.path.join(
            Contest.CONTEST_DIR,
            saved_winners[0],
        )

        with open(filename, 'r') as f:
            self.assertEqual(json.loads(f.read()), contest.get_state())
