import json
from unittest import TestCase

from mock import Mock, patch, call

from ..main import Game, Player
from ..user import User


class PlayerTestCase(TestCase):

    def setUp(self):
        self.usr = Mock(address=None, spec=User)
        self.usr.name = 'test_name'
        self.player = Player(4, 0, self.usr)

    def test_create_player(self):
        self.assertEqual(self.player.name, 'test_name')

    @patch('httplib.HTTPConnection')
    def test_get_decision(self, conn):
        response = Mock()
        response.read.return_value = json.dumps({'x': 3, 'y': 1})

        conn_inst = Mock()
        conn_inst.getresponse.return_value = response

        conn.return_value = conn_inst

        self.usr.address = '1.2.3.4:8000'
        coords = self.player.get_decision()
        self.assertEqual(coords, (3, 1))

        init_call = call('1.2.3.4:8000', timeout=15)
        game_state = json.dumps(self.player.game.get_state())
        headers = {'Content-type': 'application/json'}

        self.assertEqual(
            conn.mock_calls,
            [
                init_call,
                init_call.request('POST', '/', game_state, headers),
                init_call.getresponse(),
                init_call.getresponse().read(),
            ],
        )

    @patch('time.sleep')
    def test_no_address_fails(self, fake_sleep):
        with self.assertRaises(ValueError):
            self.player.get_decision()

    def test_play_round_failure(self):
        self.player.get_decision = Mock(side_effect=Exception('Foo'))
        self.player.game = Mock(spec=Game)
        self.player.play_round()

        self.assertEqual(self.player.correct_moves, 0)
        self.assertEqual(self.player.wrong_moves, 1)
        self.assertEqual(self.player.game.mock_calls, [call.skip_move()])

    def test_play_round_success(self):
        self.player.get_decision = Mock(return_value=(2, 0))
        self.player.game = Mock(spec=Game)
        self.player.play_round()

        self.assertEqual(self.player.correct_moves, 1)
        self.assertEqual(self.player.wrong_moves, 0)
        self.assertEqual(self.player.game.mock_calls, [call.start_move(2, 0)])
