#!/usr/bin/env python
import httplib
import json
import os
import random
import threading
import time

from datetime import datetime

from .config import MASTER_CONFIG
from .user import User
from .view import WebView

POINTS = [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]
DIRECTIONS = ['up', 'down', 'right', 'left']

def check_conn():
    if not MASTER_CONFIG.data.get('check_conn', False):
        return True

    return os.system("ping -W 5 -c 1 172.16.0.10") == 0


class Field(object):
    """
    Holds field state.
    """

    def __init__(self, x, y, random):
        self.x = x
        self.y = y
        self.random = random
        self.reset()

    def reset(self):
        self.points = self.random.choice(POINTS)
        self.direction = self.random.choice(DIRECTIONS)

    def get_state(self):
        return {
            'points': self.points,
            'direction': self.direction,
        }


class Board(object):

    def __init__(self, size, seed):
        self.size = size
        self.random = random.Random(seed)

        self.fields = [
            [Field(x, y, self.random) for x in xrange(size)]
            for y in xrange(size)
        ]

    def get_field(self, x, y):
        """
        Returns the field at the given coordinates.
        """
        return self.fields[y][x]

    def get_next_field(self, field, direction=None):
        """
        Returns next field in chain reaction and information is it last step
        in this chain reaction.
        """
        direction = field.direction or direction

        if direction == 'left':
            if field.x == 0:
                return None
            next_field = self.get_field(field.x - 1, field.y)

        elif direction == 'right':
            if field.x == (self.size - 1):
                return None
            next_field = self.get_field(field.x + 1, field.y)

        elif direction == 'up':
            if field.y == 0:
                return None
            next_field = self.get_field(field.x, field.y - 1)

        elif direction == 'down':
            if field.y == (self.size - 1):
                return None
            next_field = self.get_field(field.x, field.y + 1)

        if next_field.direction is None:
            # if next was alread cleared than go further in the same direction
            return self.get_next_field(next_field, direction)

        return next_field

    def lower_field(self, field):
        """
        When chain reaction is over fields that are 'flying' should be lowered.
        """
        new_y = field.y
        while new_y < self.size - 1:
            if self.get_field(field.x, new_y + 1).direction is not None:
                # next field below is not empty, so finish lowering
                break
            new_y += 1

        if new_y != field.y:
            next_field = self.get_field(field.x, new_y)
            # swap fields values
            field.points, next_field.points = next_field.points, field.points
            field.direction, next_field.direction = next_field.direction, field.direction

    def lower_fields(self):
        """
        Lower fields (use gravity).
        """
        for y in reversed(range(self.size - 1)):
            for x in range(self.size):
                self.lower_field(self.get_field(x, y))

    def fill_empty_fields(self):
        """
        Reset fields in empty places.
        """
        for x in range(self.size):
            for y in range(self.size):
                field = self.get_field(x, y)
                if field.direction is None:
                    field.reset()

    def get_extra_points(self):
        """
        Return extra points for the empty rows and columns.
        """
        extra_points = 0

        for x in range(self.size):
            is_empty = True
            for y in range(self.size):
                if self.get_field(x, y).direction is not None:
                    is_empty = False
                    break

            if is_empty:
                extra_points += self.size * 10

        for y in range(self.size):
            is_empty = True
            for x in range(self.size):
                if self.get_field(x, y).direction is not None:
                    is_empty = False
                    break

            if is_empty:
                extra_points += self.size * 10

        return extra_points

    def get_state(self):
        """
        Get the status of the board.
        """
        rows = []
        for y in range(self.size):
            row = []
            for x in range(self.size):
                row.append(self.get_field(x, y).get_state())
            rows.append(row)
        return rows


class Game(object):

    def __init__(self, size, seed):
        self.board = Board(size, seed)
        self.moves = 5
        self.score = 0

    def start_move(self, x, y):
        """
        Set initial values and start chain reaction.
        """

        field = self.board.get_field(x, y)
        self.moves -= 1

        self.move_score = 0
        self.move_length = 0

        self.move_next_field(field)

    def skip_move(self):
        """
        Skip the move
        """
        self.moves -= 1

    def move_next_field(self, start_field):
        """
        One step in chain reaction.
        """
        next_field = self.board.get_next_field(start_field)
        start_field.direction = None
        self.move_score += start_field.points
        self.move_length += 1

        if next_field is None:
            self.finish_move()
        else:
            self.move_next_field(next_field)

    def update_score(self):
        """
        Update game score.
        """
        self.score += self.move_score

        threshold = (self.score // (5*self.board.size**2)) + self.board.size - 1
        if self.move_length > threshold:
            self.moves += self.move_length - threshold

    def finish_move(self):
        """
        Finish the move.
        """
        self.move_score += self.board.get_extra_points()

        self.board.lower_fields()
        self.board.fill_empty_fields()

        self.update_score()

    def get_state(self):
        """
        Get the status of the game.
        """
        return {
            'score': self.score,
            'moves': self.moves,
            'board': self.board.get_state(),
        }


class Player(object):

    def __init__(self, size, seed, user):
        self.user = user

        self.wrong_moves = 0
        self.correct_moves = 0

        self.game = Game(size, seed)

    @property
    def name(self):
        return self.user.name

    def get_decision(self):
        timeout = max(15 - self.wrong_moves * 3, 1)

        if not self.user.address:
            self.user.sync(timeout=timeout)

        if not self.user.address:
            raise ValueError('Address not provided!')


        data = json.dumps(self.game.get_state())
        headers = {'Content-type': 'application/json'}

        conn = httplib.HTTPConnection(self.user.address,
                                      timeout=timeout)
        conn.request('POST', '/', data, headers)

        resp = conn.getresponse()
        decision = json.loads(resp.read().decode('utf-8'))

        x, y = decision['x'], decision['y']

        assert isinstance(x, int)
        assert isinstance(y, int)
        assert 0 <= x < self.game.board.size
        assert 0 <= y < self.game.board.size

        return x, y

    def handle_wrong_decision(self):
        self.wrong_moves += 1
        self.correct_moves = 0

        self.game.skip_move()
        print u'Player {0}: Wrong decision'.format(self.name)

    def handle_correct_decision(self, x, y):
        self.correct_moves += 1
        if self.correct_moves >= 5:
            self.wrong_moves = 0

        self.game.start_move(x, y)

    def play_round(self):
        try:
            x, y = self.get_decision()
        except Exception, e:
            import traceback
            traceback.print_exc(e)
            if check_conn():
                self.handle_wrong_decision()
        else:
            self.handle_correct_decision(x, y)

    def get_state(self):
        return {
            'name': self.name,
            'score': self.game.score,
            'moves': self.game.moves,
        }

    def is_active(self):
        return self.game.moves > 0


class Contest(object):

    CONTEST_DIR = os.path.normpath(
        os.path.join(os.path.dirname(__file__), '..', 'contests')
    )

    def __init__(self, data, seed=None):
        self.round = 0

        size = 6
        seed = random.getrandbits(128) if seed is None else seed
        self.players = [
            self.create_player(size, seed, item)
            for item in data
        ]
        self.view = WebView(host='0.0.0.0', port=5000,
                            contest=self)
        self.audience_present = threading.Event()

    def begin(self):
        self.audience_present.set()

    def create_player(self, size, seed, user):
        return Player(size, seed, user)

    def is_active(self):
        for player in self.players:
            if player.is_active():
                return True
        return False

    def get_active_players(self):
        return [player for player in self.players if player.is_active()]

    def play_round(self):
        threads = []
        for player in self.get_active_players():
            thread = threading.Thread(target=player.play_round)
            threads.append(thread)

        print 'starting round {0} - {1} players are active'.format(
            self.round, len(threads))

        while not check_conn():
            pass

        for thread in threads:
            thread.start()

        # wait until all players will finish move
        for thread in threads:
            thread.join()

        self.round += 1
        self.view.resource_changed('contest_state')
        time.sleep(1)

    def save_winners(self):
        if not os.path.exists(self.CONTEST_DIR):
            os.makedirs(self.CONTEST_DIR)
        filename = datetime.now().isoformat()
        with open(os.path.join(self.CONTEST_DIR, filename), 'w') as f:
            f.write(json.dumps(self.get_state()))

    def wait_for_game(self):
        if MASTER_CONFIG.data and MASTER_CONFIG.data.get('block_start', False):
            raw_input('Press any key to start...')
            return

        print 'The game will commence after the first pageload.'
        while not self.audience_present.wait(1):
            pass

    def run(self):
        self.view.start()
        try:
            print '*' * 10
            self.wait_for_game()
            while self.is_active():
                self.play_round()
            self.save_winners()
            print '*' * 10
            raw_input('The game is complete. Press enter to end.')
        except KeyboardInterrupt:
            print '*' * 10
            raw_input('Manual interrupt. Press enter to end.')
        finally:
            self.view.stop()
            print '*' * 10
            print 'Game ended.'

    def get_state(self):
        self.players.sort(key=lambda p: p.game.score, reverse=True)
        return {
            'round': self.round,
            'is_active': self.is_active(),
            'players': [
                player.get_state()
                for player in self.players
            ],
        }


def start_contest(data):
    contest = Contest(data)
    contest.run()


if __name__ == '__main__':
    users = User.get_all()

    if not users:
        print '*' * 10
        print 'No players added. Terminating...'
        raise SystemExit(0)

    try:
        start_contest(users)
    except (KeyboardInterrupt, SystemExit):
        print 'Exiting...'
