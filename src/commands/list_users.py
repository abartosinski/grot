from ..user import User


def prompt_list_users():
    query = raw_input(
        'Provide an optional search query: '
    ).decode('utf-8').lower()

    users = User.get_all()

    if query:
        users = filter(lambda u: u.name.lower().find(query) != -1, users)

    if not users:
        print 'No users found.'
        return

    print '{:<30} {:<20}\n'.format(
        'NAME', 'ADDRESS',
    )

    for user in users:
        print u'{:<30} {:<20}'.format(
            user.name,
            user.address if user.address else 'N/A',
        )

    print '\nUsers found ({}):'.format(len(users))
    print 'Properly configured - {}'.format(len(filter(
        lambda u: u.address, users
    )))

if __name__ == '__main__':
    prompt_list_users()
