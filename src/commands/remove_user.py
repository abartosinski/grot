from ..config import PLAYER_CONFIG


def prompt_remove_user():
    name = None

    while not name:
        name = raw_input(
            'Provide the name of the player to remove: '
        ).decode('utf-8')

    old_num = len(PLAYER_CONFIG.data)
    PLAYER_CONFIG.data = [p for p in PLAYER_CONFIG.data if p['name'] != name]

    if old_num == len(PLAYER_CONFIG.data):
        print u'User {} not found.'.format(name)
        return

    PLAYER_CONFIG.save()
    print u'User {} successfully removed.'.format(name)


if __name__ == '__main__':
    prompt_remove_user()
