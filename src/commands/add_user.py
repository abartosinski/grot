from ..config import PLAYER_CONFIG


def prompt_add_user():
    usernames = [p['name'] for p in PLAYER_CONFIG.data]

    username = None
    address = None

    while not username:
        username = raw_input('Username: ').decode('utf-8')

    if username in usernames:
        print u'User {} already exists'.format(username)
        return

    while not address:
        address = raw_input('Address: ')

    PLAYER_CONFIG.data.append({
        "name": username,
        "address": address,
    })
    PLAYER_CONFIG.save()

    print u'Added user {} with address {}'.format(
        username,
        address,
    )


if __name__ == '__main__':
    prompt_add_user()
