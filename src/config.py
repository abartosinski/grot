import json
import os


class Config(object):

    BASE_PATH = os.path.normpath(
        os.path.join(os.path.dirname(__file__), '..', 'config')
    )

    def __init__(self, name, default=None):
        self.ensure_base_path()
        self.name = name

        if os.path.isfile(self.path):
            with open(self.path, 'r') as f:
                self.data = json.loads(f.read())
        else:
            self.data = default

    @classmethod
    def ensure_base_path(cls):
        base_path = cls.BASE_PATH

        if not os.path.exists(base_path):
            os.makedirs(base_path)

    @property
    def path(self):
        return os.path.join(self.BASE_PATH, self.name)

    def save(self):
        with open(self.path, 'w') as f:
            f.write(json.dumps(self.data))


MASTER_CONFIG = Config('master')
PLAYER_CONFIG = Config('players', default=[])
